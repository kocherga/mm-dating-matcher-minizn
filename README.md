# mm-dating-matcher-minizn

A discrete optimisation model for MM dating. Given a `data.dzn`, computes optimal groups. Written in [MiniZinc](https://www.minizinc.org/).

```sh
minizinc model.mzn data.dzn --time-limit 1000
```

JSON input and output (`--output-mode json`) is available.
