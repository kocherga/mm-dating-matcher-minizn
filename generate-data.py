#!/usr/bin/env python3
def rand_solver_data(n=10, saveto="data.dzn"):
    result = []
    for i in range(n):
        result.append('|\n')
        for j in range(n):
            if i == j:
                vote = 0
            else:
                import random
                vote = random.randint(0, 2)
            result.append(f"{['Y', 'O', 'N'][vote]},")
    result.append('|')
    unames = ",".join(map(lambda a: f"p{a}", range(n)))
    dataset = "".join(result)
    file = """
SCORES = {Y, O, N};
people={%s};
groups_of_n = 5;
marks=[%s]
    """.strip() % (unames, dataset)
    with open(saveto, mode="w") as f:
        f.write(file)

rand_solver_data()
